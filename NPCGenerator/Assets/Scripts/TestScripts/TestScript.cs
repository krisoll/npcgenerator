﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestScript : MonoBehaviour
{
    public int numCharacter;
    public float separation;
    public GameObject CharacterPrefab;
    private void Start()
    {
        float cont = 0;
        for(int i = 0; i < numCharacter; i++)
        {
            GameObject g = Instantiate(CharacterPrefab);
            g.transform.position = transform.position + transform.right * cont;
            cont += separation;
        }
    }
}
