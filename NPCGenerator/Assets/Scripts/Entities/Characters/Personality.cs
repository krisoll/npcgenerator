﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[System.Serializable]
public class Personality
{
    public string name;
}
[System.Serializable]
public class Quirks
{
    public string name;
}