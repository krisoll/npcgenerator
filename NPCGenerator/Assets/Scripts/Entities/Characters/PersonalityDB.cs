﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "PersonalitiesDB", menuName = "Databases/PersonalitiesDB")]
public class PersonalityDB : ScriptableObject
{
    public List<Personality> personalities;
}
