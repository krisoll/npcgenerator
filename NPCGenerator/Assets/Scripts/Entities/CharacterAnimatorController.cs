﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterAnimatorController : MonoBehaviour
{
    private Character character;
    private Animator anim;
    public float velocity;
    private bool moving;
    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {

        anim.SetFloat("Velocity", velocity);
        if (velocity > 0.01)
        {
            if (!moving)
            {
                moving = true;
            }
        }
        else if (velocity < 0.01)
        {
            if (moving)
            {
                int r = Random.Range(0, 2);
                anim.SetInteger("Random", r);
                moving = false;
            }
        }
    }
}
