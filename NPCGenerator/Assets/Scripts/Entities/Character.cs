﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pathfinding;
using Pathfinding.RVO;
public class Character : MonoBehaviour,IActionable
{
    public Personality pers;
    public string charName;
    public float velocity;
    public Seeker seeker;
    public RVOController rvo;
    public RichAI richAI;
    public Vector2 moveDelay;
    public Vector2 maxDistance;
    public SkinnedMeshRenderer[] baseMeshes;
    public SkinnedMeshRenderer[] detailMeshes;
    [SerializeField] private CharacterAnimatorController anim;
    [SerializeField] private bool moving;
    private Material mat;
    private void Start()
    {
        pers = GameManager.Instance.GetRandomPersonality();
        charName = DataManager.Instance.GetRandomName();
        //StartCalculation();
        StartCoroutine(StartTravelling());
        ChangeMeshesColor();
    }
    void ChangeMeshesColor()
    {
        Color baseM = Random.ColorHSV();
        Color baseD = Random.ColorHSV();
        foreach (SkinnedMeshRenderer m in baseMeshes)
        {
            mat = m.material;
            mat.SetColor("_Color", baseM);
        }
        foreach(SkinnedMeshRenderer m in detailMeshes)
        {
            mat = m.material;
            mat.SetColor("_Color", baseD);
        }
    }
    void Update()
    {
        anim.velocity = richAI.velocity.magnitude;
    }

    IEnumerator StartTravelling()
    {
        float delay = Random.Range(moveDelay.x, moveDelay.y);
        yield return new WaitForSeconds(delay);
        float diffMax = maxDistance.y - maxDistance.x;
        float x = Random.Range(-diffMax, diffMax) * maxDistance.x;
        float z = Random.Range(-diffMax, diffMax) * maxDistance.x;
        Vector3 finalPos = new Vector3(transform.position.x + x, transform.position.y, transform.position.z + z);
        richAI.destination = finalPos;
        while (!richAI.reachedEndOfPath)
        {
            yield return null;
        }
        StartCoroutine(StartTravelling());
    }
    IEnumerator RotateToQuaternion(Quaternion rot, float rotVelocity, bool onlyHorizontal)
    {
        if (onlyHorizontal)
        {
            Vector3 ro = rot.eulerAngles;
            ro = new Vector3(transform.eulerAngles.x, ro.y, ro.z);
            rot = Quaternion.Euler(ro);
        }
        while (rot != transform.rotation)
        {
            transform.rotation = Quaternion.RotateTowards(transform.rotation, rot, rotVelocity * Time.deltaTime);
            yield return null;
        }
    }
    [ContextMenu("Show Dialogue")]
    void ShowDialogueAndName()
    {
        UIManager.Instance.WriteDialogue(charName, "Hola mi nombre es " + charName + " y soy de personalidad " + pers.name, 0.05f);
    }
    public void Action()
    {
    }
    public void Action(PlayerController player)
    {
        ShowDialogueAndName();
        StopAllCoroutines();
        richAI.destination = transform.position;
        richAI.updatePosition = false;
        richAI.updateRotation = false;
        Vector3 relativePos = player.transform.position - transform.position;
        StartCoroutine(RotateToQuaternion(Quaternion.LookRotation(relativePos), 180,true));
    }
}
