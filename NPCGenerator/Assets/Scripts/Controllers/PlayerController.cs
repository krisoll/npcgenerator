﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public GameObject player;
    public GameObject Graphics;
    public float velocity;
    public Rigidbody rigid;
    [SerializeField] private LayerMask actionMask;
    [SerializeField] private Vector3 box;
    [SerializeField] private float projection;

    private CameraController cam;

    private Vector3 actionboxCenter { get { return transform.position + (Graphics.transform.forward * projection); } }
    private void OnDrawGizmos()
    {
        Gizmos.color = new Color(0, 1, 0, 0.3f);
        Gizmos.DrawCube(actionboxCenter, box);
    }
    private void Start()
    {
        cam = GetComponent<CameraController>();
        InputManager.Instance.OnActionPressed += OnAction;
    }
    private void FixedUpdate()
    {
        Graphics.transform.LookAt(CalcDirection() + transform.position);
        rigid.AddForce(CalcDirection() * velocity,ForceMode.Force);
        
    }
    private Vector3 CalcDirection()
    {
        Vector3 vec = (cam.GetFoward() * InputManager.Instance.MoveAxis.y) + (cam.GetRight() * InputManager.Instance.MoveAxis.x);
        return vec;
    }
    private void OnAction()
    {
        Collider[] isAction = Physics.OverlapBox(actionboxCenter, box, Quaternion.identity, actionMask);
        if (isAction.Length>0)
        {
            for (int i = 0; i < isAction.Length; i++)
            {
                IActionable act = isAction[i].GetComponent<IActionable>();
                if (act != null) act.Action(this);
            }
        }
    }
}
