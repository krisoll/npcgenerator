﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    [SerializeField] private GameObject cameraPrefab;
    [SerializeField] private Vector3 cameraInitialOffset;
    private GameObject cameraObj;
    private Camera playerCamera;
    private PlayerController player;
    [SerializeField] private LayerMask mask;

    private  Transform target;
    public float distance = 5.0f;
    public float xSpeed = 120.0f;
    public float ySpeed = 120.0f;

    public float yMinLimit = -20f;
    public float yMaxLimit = 80f;

    public float distanceMin = .5f;
    public float distanceMax = 15f;

    float x = 0.0f;
    float y = 0.0f;

    // Use this for initialization
    void Start()
    {
        //Fill Camera
        cameraObj = Instantiate(cameraPrefab);
        playerCamera = cameraObj.GetComponent<Camera>();
        CameraManager.Instance.mainCamera = playerCamera;
        
        //Fill Player
        player = GetComponent<PlayerController>();
        target = player.player.transform;
        Vector3 angles = cameraObj.transform.eulerAngles;

        x = angles.y;
        y = angles.x;
    }

    void LateUpdate()
    {
        if (target)
        {
            x += InputManager.Instance.MouseAxis.x * xSpeed * InputManager.Instance.MouseSensitivity * distance;
            y -= InputManager.Instance.MouseAxis.y * ySpeed * InputManager.Instance.MouseSensitivity;

            y = ClampAngle(y, yMinLimit, yMaxLimit);

            Quaternion rotation = Quaternion.Euler(y, x, 0);

            distance = Mathf.Clamp(distance - Input.GetAxis("Mouse ScrollWheel") * 5, distanceMin, distanceMax);

            RaycastHit hit;
            if (Physics.Linecast(target.position, cameraObj.transform.position, out hit, mask))
            {
                distance -= hit.distance;
            }
            Vector3 negDistance = new Vector3(0.0f, 0.0f, -distance);
            Vector3 position = rotation * negDistance + target.position;

            cameraObj.transform.rotation = rotation;
            cameraObj.transform.position = position;
        }
    }
    public Vector3 GetFoward()
    {
        return new Vector3(cameraObj.transform.forward.x, 0, cameraObj.transform.forward.z);
    }
    public Vector3 GetRight()
    {
        return new Vector3(cameraObj.transform.right.x, 0, cameraObj.transform.right.z);
    }
    public static float ClampAngle(float angle, float min, float max)
    {
        if (angle < -360F)
            angle += 360F;
        if (angle > 360F)
            angle -= 360F;
        return Mathf.Clamp(angle, min, max);
    }
}
