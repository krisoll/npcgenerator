﻿using UnityEngine;
[System.Serializable]
public class PlayerLiveData
{
    public GameObject playerObject;
    public int health;
    public int stamina;
}
