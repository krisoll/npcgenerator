﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[System.Serializable]
public class NameData
{
    public Names names;
    public NameData(string json)
    {
        names = JsonUtility.FromJson<Names>(json);
    }
    [System.Serializable]
    public class Names
    {
        public List<string> names;
    }
}
