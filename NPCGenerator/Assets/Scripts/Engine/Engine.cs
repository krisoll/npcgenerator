﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
public class Engine : MonoBehaviour
{

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(Initialize());
    }
    private IEnumerator Initialize()
    {
        AddManager<GameManager>();
        AddManager<InputManager>();
        AddManager<TimeManager>();
        AddManager<CameraManager>();
        AddManager<DataManager>();
        AddManager<UIManager>();
        yield return null;
    }

    void AddManager<T>()
    {
        Type t = typeof(T);
        var b = gameObject.AddComponent(t);
    }
}
