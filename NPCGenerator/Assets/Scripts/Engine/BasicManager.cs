﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
public class BasicManager<T> : MonoBehaviour where T: BasicManager<T>
{
    public static T Instance;
    private bool initialized;
    protected virtual void Awake()
    {
        if (Instance == null) Instance = this as T;
        Initialize();
    }
    protected virtual void Initialize()
    {
        initialized = true;
    }
    public bool IsInitialized()
    {
        return initialized;
    }
}
