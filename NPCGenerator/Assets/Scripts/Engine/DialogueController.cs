﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public class DialogueController : MonoBehaviour
{
    public GameObject panel;
    public TextMeshProUGUI dialogue;
    public TextMeshProUGUI charaName;
    public Animator anim;
    public float defaultDissapearValue;
    //Current Values
    private char[] currentChars;
    private float charDelay;
    private Coroutine currentCorroutine;

    public void WriteText(string charName,string text,float charDelay)
    {
        dialogue.SetText("");
        charaName.SetText(charName);
        currentChars = text.ToCharArray();
        this.charDelay = charDelay;
        if (currentCorroutine != null) StopCoroutine(currentCorroutine);
        currentCorroutine = StartCoroutine(WriteCoroutine());
        anim.SetTrigger("Appear");
    }

    IEnumerator WriteCoroutine()
    {
        string s = "";
        for (int i = 0; i < currentChars.Length; i++)
        {
            s += currentChars[i];
            dialogue.SetText(s);
            yield return new WaitForSeconds(charDelay);
        }
        yield return new WaitForSeconds(defaultDissapearValue);
        anim.SetTrigger("Dissapear");
    }
}
