﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
public class InputManager : BasicManager<InputManager>
{
    public Vector2 MoveAxis;
    public Vector2 MouseAxis;
    public float MouseSensitivity = 0.02f;
    public Action OnActionPressed = delegate { };
    public void Update()
    {
        MouseAxis = new Vector2(Input.GetAxis("Mouse X"), Input.GetAxis("Mouse Y"));
        MoveAxis = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
        if (Input.GetButtonDown("Fire1")) OnActionPressed();
    }
}
