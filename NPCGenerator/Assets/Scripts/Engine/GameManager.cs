﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : BasicManager<GameManager>
{
    [HideInInspector]public PersonalityDB personalityDB;

    protected override void Initialize()
    {
        LoadPersonalities();
        base.Initialize();
    }

    public Personality GetRandomPersonality()
    {
        int rand = Random.Range(0, personalityDB.personalities.Count);
        return personalityDB.personalities[rand];
    }

    void LoadPersonalities()
    {
        personalityDB = Resources.Load<PersonalityDB>(GetDBPath() + GameConstants.Personalities);
    }
    string GetDBPath()
    {
        return "Databases/";
    }
}
