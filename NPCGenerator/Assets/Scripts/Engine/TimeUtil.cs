﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
public class TimeUtil
{
    public static int epoch = 2020;

    public static DateTime Now()
    {
        return DateTime.UtcNow;
    }

    public static int LeapYearsSinceEpoch(DateTime date)
    {
        int cont = 0;
        for(int i = epoch; i < date.Year; i++)
        {
            if (DateTime.IsLeapYear(i)) cont++;
        }
        return cont;
    }

    public static int DateToSeconds(DateTime date)
    {
        int years = date.Year - epoch;
        int days = date.DayOfYear + LeapYearsSinceEpoch(date);
        int hour = date.Hour;
        int minute = date.Minute;
        int second = date.Second;

        int yearsInSeconds = years * 60 * 60 * 24 * 365;
        int daysInSeconds = days * 60 * 60 * 24;
        int hourInSeconds = hour * 60 * 60;
        int minuteInSeconds = minute * 60;

        return yearsInSeconds + daysInSeconds + hourInSeconds + minuteInSeconds + second;
    }
}
