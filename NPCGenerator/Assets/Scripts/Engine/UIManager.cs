﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public class UIManager : BasicManager<UIManager>
{
    public Canvas canvas;
    public DialogueController dialogue;
    private void Start()
    {
        GameObject canvasPrefab = Resources.Load<GameObject>("Engine/Canvas");
        canvas = Instantiate(canvasPrefab).GetComponent<Canvas>();
        dialogue = canvas.GetComponent<DialogueController>();
    }
    public void WriteDialogue(string charName,string text, float charDelay)
    {
        dialogue.WriteText(charName,text, charDelay);
    }
}
