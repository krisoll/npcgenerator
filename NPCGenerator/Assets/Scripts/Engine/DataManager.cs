﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataManager : BasicManager<DataManager>
{
    public PlayerLiveData livePlayer;
    public PlayerData data;
    public NameData GargoyleNames;
    private void Start()
    {
        LoadNameData();
    }
    void LoadNameData()
    {
        TextAsset tex = Resources.Load<TextAsset>("Data/Names");
        Debug.Log(tex.text);
        GargoyleNames = new NameData(tex.text);
    }
    public string GetRandomName()
    {
        int r = Random.Range(0, GargoyleNames.names.names.Count);
        return GargoyleNames.names.names[r];
    }
    public void LoadData()
    {

    }
    public void SaveData()
    {
        string playerData = JsonUtility.ToJson(data);
    }
}
